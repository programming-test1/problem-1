import edu.wpi.first.wpilibj2.command.CommandBase;
public class Turn extends CommandBase{
    private DriveCar d;
    public Turn(DriveCar d){
        this.d = d;
        addRequirements(d);
    }
    @Override;
    public void initialize(){
        d.drive(0.5,90);//90 degree turn at half speed
    } 
    @Override;
    public void end(boolean interrupted){
        d.stop();
    }
}