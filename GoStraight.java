import edu.wpi.first.wpilibj2.command.CommandBase;
public class GoStraight extends CommandBase{
    private DriveCar d;
    public GoStraight(DriveCar d){
        this.d = d;
        addRequirements(d);
    }
    @Override;
    public void initialize(){
        d.drive(0.5,0);
    }
    @Override;
    public void end(boolean interrupted){
        d.stop();
    }
}