import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

public class DriveCar extents SubsystemBase{
    private CANSparkMax m1 = new CANSparkMax(1, MotorType.kBrushless);
    private CANSparkMax m2 = new CANSparkMax(2, MotorType.kBrushless);
    private DiffrentialDrive d = new DifferentialDrive(m1, m2);
    public DriveCar(){
        m1.setInverted(false);
        m2.setInverted(false);
    }
    public drive(double speed, double rotation){
        d.drive(speed, rotation);
    }
    public void stop(){
        d.stopMotor();
    }
}
