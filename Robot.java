import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
public class Robot{
    private Joystick j = new Joystick(0);//connected to port 0
    private ArcadeDrive a = new ArcadeDrive();
    public Robot(){
        addJoystick();
    }
    private void addJoystick(){
        JoystickButton go = new JoystickButton(j,1);//button 1
        go.whenPressed(new ArcadeDrive(a));
    }
}

