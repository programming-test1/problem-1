import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
public class ArcadeDrive extents CommandBase{
    private ArcadeDrive arcadeDrive;
    public ArcadeDrive(DriveCar driveCar){
        this.arcadeDrive = arcadeDrive;
        addRequirements(arcadeDrive);
    }
    @Override
    public void initialize(){
        CommandScheduler.getInstance().schedule(new GoStraight(arcadeDrive),10);//go straight for 10 secs
        CommandScheduler.getInstance().schedule(new Turn(arcadeDrive),10);//turn 90 degrees after 10 secs
    }
    @Override
    public void isFinished(){
        return true;
    }
}
